﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureCrypt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String encrypt = txtplain.Text;
            String encrypted;

            //This will look for a lowercase letter and convert it to Uppercase. -Simeon
            //This algorithm has been ordered using a key of Joker and Batman developed by Jacob
            encrypted = encrypt
                .Replace("a", "G")
                .Replace("b", "Q")
                .Replace("c", "Z")
                .Replace("d", "F")
                .Replace("e", "S")
                .Replace("f", "O")
                .Replace("g", "D")
                .Replace("h", "L")
                .Replace("i", "W")
                .Replace("j", "N")
                .Replace("k", "J")
                .Replace("l", "K")
                .Replace("m", "P")
                .Replace("n", "R")
                .Replace("o", "A")
                .Replace("p", "M")
                .Replace("q", "T")
                .Replace("r", "V")
                .Replace("s", "E")
                .Replace("t", "B")
                .Replace("u", "U")
                .Replace("v", "X")
                .Replace("w", "H")
                .Replace("x", "C")
                .Replace("y", "I")
                .Replace("z", "Y");

            //This will output the ciphertext -Simeon
            txtencrypted.Text = encrypted;
            button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String decrypt = txtencrypted.Text;
            String decrypted;

            //This will look for an Uppercase letter and convert it to lowercase. -Simeon
            decrypted = decrypt
                .Replace("G", "a")
                .Replace("Q", "b")
                .Replace("Z", "c")
                .Replace("F", "d")
                .Replace("S", "e")
                .Replace("O", "f")
                .Replace("D", "g")
                .Replace("L", "h")
                .Replace("W", "i")
                .Replace("N", "j")
                .Replace("J", "k")
                .Replace("K", "l")
                .Replace("P", "m")
                .Replace("R", "n")
                .Replace("A", "o")
                .Replace("M", "p")
                .Replace("T", "q")
                .Replace("V", "r")
                .Replace("E", "s")
                .Replace("B", "t")
                .Replace("U", "u")
                .Replace("X", "v")
                .Replace("H", "w")
                .Replace("C", "x")
                .Replace("I", "y")
                .Replace("Y", "z");


            //This will output decrypted text. -Simeon
            txtdecrypted.Text = decrypted;
            button1.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //create an array of characters of the plain text that was entered
            char[] characters = txtplain.Text.ToCharArray();
            //store the hashed text in a variable
            string hashedText = hash(characters);
            //display the hashed text
            textBox1.Text = hashedText;

        }

        //hash function
        public string hash(char[] plain)
        {
            //get the length of the array
            int h = plain.Length - 1;
            //initialize the hashed text variable
            string hashedText = "";
            //run through the plain array
            for (int i = 0; i <= h; i++)
            {
                if ((plain[i] == 'A') || (plain[i] == 'a'))
                {
                    //add to the hashed text
                    hashedText += "XV2B";
                }
                else if ((plain[i] == 'B') || (plain[i] == 'b'))
                {
                    //add to the hashed text
                    hashedText += "QFAR";
                }
                else if ((plain[i] == 'C') || (plain[i] == 'c'))
                {
                    hashedText += "SN10";

                }
                else if ((plain[i] == 'D') || (plain[i] == 'd'))
                {
                    hashedText += "LPT9";
                }
                else if ((plain[i] == 'E') || (plain[i] == 'e'))
                {
                    hashedText += "GMRG";
                }
                else if ((plain[i] == 'F') || (plain[i] == 'f'))
                {
                    hashedText += "5O0P";
                }
                else if ((plain[i] == 'G') || (plain[i] == 'g'))
                {
                    hashedText += "5O0P";
                }
                else if ((plain[i] == 'H') || (plain[i] == 'h'))
                {
                    hashedText += "Ahk7";
                }
                else if ((plain[i] == 'I') || (plain[i] == 'i'))
                {
                    hashedText += "JL9L";
                }
                else if ((plain[i] == 'J') || (plain[i] == 'j'))
                {
                    hashedText += "QXYB";
                }
                else if ((plain[i] == 'K') || (plain[i] == 'k'))
                {
                    hashedText += "H4F3";
                }
                else if ((plain[i] == 'L') || (plain[i] == 'l'))
                {
                    hashedText += "058u";
                }
                else if ((plain[i] == 'M') || (plain[i] == 'm'))
                {
                    hashedText += "RZ1P";
                }
                else if ((plain[i] == 'N') || (plain[i] == 'n'))
                {
                    hashedText += "MTv3";
                }
                else if ((plain[i] == 'O') || (plain[i] == 'o'))
                {
                    hashedText += "DNFR";
                }
                else if ((plain[i] == 'P') || (plain[i] == 'p'))
                {
                    hashedText += "Z8GV";
                }
                else if ((plain[i] == 'Q') || (plain[i] == 'q'))
                {
                    hashedText += "9WV0";
                }
                else if ((plain[i] == 'R') || (plain[i] == 'r'))
                {
                    hashedText += "A5FI";
                }
                else if ((plain[i] == 'S') || (plain[i] == 's'))
                {
                    hashedText += "YIZT";
                }
                else if ((plain[i] == 'T') || (plain[i] == 't'))
                {
                    hashedText += "IHB7";
                }
                else if ((plain[i] == 'U') || (plain[i] == 'u'))
                {
                    hashedText += "DTSN";
                }
                else if ((plain[i] == 'V') || (plain[i] == 'v'))
                {
                    hashedText += "8Y4L";
                }
                else if ((plain[i] == 'W') || (plain[i] == 'w'))
                {
                    hashedText += "H6J3";
                }
                else if ((plain[i] == 'X') || (plain[i] == 'x'))
                {
                    hashedText += "0NO5";
                }
                else if ((plain[i] == 'Y') || (plain[i] == 'y'))
                {
                    hashedText += "ARNG";
                }
                else if ((plain[i] == 'Z') || (plain[i] == 'z'))
                {
                    hashedText += "H0CO";
                }
                else if (plain[i] == ' ')
                {
                    hashedText += "VNK5";
                }
                else if (plain[i] == ',')
                {
                    hashedText += "75P4";
                }
                else if (plain[i] == '.')
                {
                    hashedText += "38XB";
                }
                else if (plain[i] == '?')
                {
                    hashedText += "ZS49";
                }
                else
                {
                    hashedText += "2QPR";
                }

            }
            //return the hashed text
            return hashedText;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtplain.Text = null;
            txtencrypted.Text = null;
            txtdecrypted.Text = null;
            textBox1.Text = null;

            button1.Enabled = true;
        }
    }
}
