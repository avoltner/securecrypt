﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureCrypt
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //set the username and password
            string username = textBox1.Text;
            string password = textBox2.Text;
            //check the username and password
            if ((username == "ist325") && (password == "test"))
            {
                //show the other form when the login is correct
                new Form1().Show();
                //hide this form
                this.Hide();
            }
        }
    }
}
